// New Stuff

var statestaken = [];
var record = {"username": "Nobody", "score": 0};
var current_state = "";
var current_capital = "";
var current_population = 0;
var randomnum = 0;
var quizOn = 1;
var correctAnswers = 0;
var capitalheader = document.getElementById("header-capital");
var populationheader = document.getElementById("header-population");
var scoreheader = document.getElementById("header-current");
var recordheader = document.getElementById("header-record");
var submitButton = document.getElementById("question-submit");
var capitalInput = document.getElementById("input-text");
onload();

function onload(){
    // Configure states taken 
    for(var i = 0; i < 50; i++){
        statestaken.push(1);
    }

    // Get first random capital
    rolldice();

    // Get state info
    makeNetworkCallToCapitalApi(randomnum + 1);
}

function handleEndOfGame(trigger){
    quizOn = 0;
    var username = "";
    if(trigger==="wrong_answer"){
        username = prompt("Wrong Answer! Enter your name for the scoreboard: ", "Joe");
    }
    else if(trigger==="end"){
        username = prompt("Game Over! Enter your name for the scoreboard:", "Joe");
    }
    
    // Set new record
    if(correctAnswers >= record['score']){
        record['score'] = correctAnswers;
        record['username'] = username;
        recordheader.textContent = "The current record is " + record['score'] + ", held by " + record['username'] + ".";
    }

    // Store Data in json file
    // const json_data = JSON.stringify(data);
    // console.log(json_data);
    //fs.writeFile('./results.json', json_data);

    // Move to scoreboard page
    // window.location.href='./index.html';
    // Reset
    reset();
}

function onClick(){
    // Get answer
    var capital = String(capitalInput.value);

    // Check Answer
    if(capital.toLowerCase() !== current_capital.toLowerCase()){
        handleEndOfGame("wrong_answer");
        return;
    }

    // Update Score
    correctAnswers++;
    scoreheader.textContent = "Current Score: " + correctAnswers;

    // Get random capital
    rolldice();
    
    // Get state info
    makeNetworkCallToCapitalApi(randomnum + 1);
}

function reset(){
    for(var i = 0; i < 50; i++){
        statestaken[i] = 1;
    }

    // Clean up score
    quizOn = 1; 
    correctAnswers = 0;
    scoreheader.textContent = "Current Score: " + 0;

    // Get random capital
    rolldice();
    
    // Get state info
    makeNetworkCallToCapitalApi(randomnum + 1);
}

function rolldice(){
    // Check if there are any available states
    var available = 0;
    for(var i = 0; i < 50; i++){
        if(statestaken[i] === 1){
            available = 1;
            break;
        }
    }

    // Check if there are any available states
    if(!available){
        handleEndOfGame("end");
        return;
    }
    
    // Get random num
    var dice = Math.floor(Math.random() * 50);

    // Ensure capital is not taken already
    while(statestaken[dice] === 0){
        dice = Math.floor(Math.random() * 50);
    }

    // Update and return
    statestaken[dice] = 0;
    randomnum = dice;
}

function makeNetworkCallToCapitalApi(capitalID){
    var url = 'http://student13.cse.nd.edu:51022/capitals/' + capitalID;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true); 
    xhr.onload = function(e){
        var response_json = JSON.parse(xhr.responseText);
        current_capital = response_json['capital'];
        current_state = response_json['state'];

        capitalheader.textContent = "What is the capital of " + current_state + "?";    

        var image = document.getElementById('statemap');
        var stateimagesrc = "./statemaps/" + response_json['state'] + ".png";
        image.setAttribute("src", stateimagesrc);

        makeNetworkCallToPopulationsApi(capitalID);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function makeNetworkCallToPopulationsApi(capitalID){
    var url = "https://datausa.io/api/data?drilldowns=State&measures=Population&year=latest";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        var response_json = JSON.parse(xhr.responseText);
        var current_population = response_json['data'][capitalID]['Population'];
        
        
        populationheader.textContent = "It has a current population of " + parseFloat(current_population).toLocaleString('en') + " people";
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

// Old Stuff

/*
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;
var para = document.createElement("p");
var newline = document.createElement("br");
var population = document.createElement("p");
population.setAttribute("id", "population-paragraph");
para.setAttribute("id", "submit-paragraph");

var label_text = document.createTextNode("State info (from first API):");
var population_text = document.createTextNode("Population (from second API): ");

para.appendChild(label_text);
population.appendChild(population_text);

document.body.appendChild(para);
document.body.appendChild(newline);
document.body.appendChild(population);

var randomnum = 12;
document.onload(makeNetworkCallToCapitalApi(randomnum))

function getFormInfo(){
    var tl1 = document.getElementById("submit-paragraph");
	tl1.innerHTML = "State info (from first API): ";
    var tl2 = document.getElementById("population-paragraph");
	tl2.innerHTML = "Population (from second API): ";
    var capitalID = document.getElementById("capital-id").value;
    makeNetworkCallToCapitalApi(capitalID);
}

function makeNetworkCallToCapitalApi(capitalID){
    var url = 'http://student13.cse.nd.edu:51022/capitals/' + capitalID;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true); 
    xhr.onload = function(e){
        var response_json = JSON.parse(xhr.responseText);
        var capital = document.createTextNode(response_json['capital']);
        var state = document.createTextNode(", " + response_json['state']);

        var image = document.getElementById('statemap');
        var stateimagesrc = "./statemaps/" + response_json['state'] + ".png";
        image.setAttribute("src", stateimagesrc);

        para.appendChild(capital);
        para.appendChild(state);

        makeNetworkCallToPopulationsApi(capitalID);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function makeNetworkCallToPopulationsApi(capitalID){
    var url = "https://datausa.io/api/data?drilldowns=State&measures=Population&year=latest";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        var response_json = JSON.parse(xhr.responseText);
        var pop = document.createTextNode(response_json['data'][capitalID]['Year'] + ": " + response_json['data'][capitalID]['Population']);
        
        population.appendChild(pop);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}
*/