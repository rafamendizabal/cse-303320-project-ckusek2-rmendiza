# cse-30332-project-ckusek2-rmendiza

JSON specification:
https://docs.google.com/document/d/1JZKPUT2O460W4JpgG47H4bDN4ygY-LZViJ0dxRs1O_U/edit

Complexity: This project was fairly complex. This project incorporates a lot of different factors into one app. We rely on our server built in python to access the data for the states and capitals, as well as an external API to get the data for the populations of the capital cities. We were also able to incorporate a map, which will dynamically update depending on which state is being asked about. We also use a record system, which keeps track of the highest score and the name of the user who got that score. If the score is beaten, the user will be able to enter their name and the record score will be updated.
