class _capital_database:

	def __init__(self):
		self.capitals = dict() 
		self.states = dict()	
		self.capital_pop = dict()	
		
	def load_capitals(self, capital_file):
		f = open(capital_file)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			cid = int(components[0])
			cname = components[1]
			cstate = components[2]
			self.capitals[cid] = cname
			self.states[cid] = cstate
		f.close()

	def load_pop(self, pop_file):
		f = open(pop_file)
		for line in f:
			line = line.rstrip()
			components=line.split("::")
			pid = int(components[0])
			population = int(components[1])
			self.capital_pop[pid] = population
		f.close()
		
	def get_capitals(self):
		return self.capitals.keys()
	
	def get_pair(self, cid):
		try:
			cname = self.capitals[cid]
			cstate = self.states[cid]
			pair = list((cname, cstate))
		except Exception as ex:
			pair = None

		return pair
	
	def set_capital(self, cid, cap):
		self.capitals[cid] = cap[0]
		self.states[cid] = cap[1]

	def delete_capital(self, cid):
		del(self.capitals[cid])
		del(self.states[cid])
		del(self.capital_pop[cid])

	def get_pop(self, pid):
		try:
			population = self.capital_pop[pid]
		except Exception as ex:
			population = 0

		return population
	
	def zero_all_pops(self):
		for cid in self.get_capitals():
			self.capital_pop[cid] = 0
			
	def get_highest_pop(self):
		highest_pop = 0
		highest_cid = 0
		for temp_cid in self.capitals.keys():
			temp_pop = self.get_pop(temp_cid)
			if (temp_pop > highest_pop):
				highest_pop = temp_pop
				highest_cid = temp_cid
				
		return(highest_rated_mid)
