import cherrypy
import re, json
from cap_library import _capital_database

class PopController(object):

	def __init__(self, cdb=None):
		if cdb is None:
			self.cdb = _capital_database()
			self.cdb.load_pop('populations.dat')
		else:
			self.cdb = cdb

	def GET_KEY(self, capital_id):
		output = {'result' : 'success'}
		capital_id = int(capital_id)
		output['capital_id'] = movie_id
		output['population'] = self.cdb.get_pop(capital_id)

		return json.dumps(output)
