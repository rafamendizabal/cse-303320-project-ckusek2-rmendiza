import unittest
import requests
import json

class TestKey(unittest.TestCase):
	
	SITE_URL = 'http://localhost:51022'
	print("testing for server: " + SITE_URL)
	CAPITALS_URL = SITE_URL + '/capitals/'
	POP_URL = SITE_URL + '/populations/'
	RESET_URL = SITE_URL + '/reset/'
	
	def reset_data(self):
		m = {}
		r = requests.put(self.RESET_URL, data = json.dumps(m))
		
	def is_json(self, resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False
		
	def test_capitals_get_key(self):
		self.reset_data()
		capital_id = 6
		r = requests.get(self.CAPITALS_URL + str(capital_id))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['capital'], 'Denver')
		self.assertEqual(resp['state'], 'Colorado')	
		
	def test_pop_get_key(self):
		self.reset_data()
		capital_id = 6

		r = requests.get(self.POP_URL + str(capital_id))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['population'], 716492)
		self.assertEqual(resp['id'], capital_id)
		
	def test_capitals_put_key(self):
		self.reset_data()
		capital_id = 6
		
		r = requests.get(self.CAPITALS_URL + str(capital_id))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['capital'], 'Denver')
		self.assertEqual(resp['state'], 'Colorado')
		
		m = {}
		m['capital'] = 'Washington DC'
		m['state'] = 'USA'
		r = requests.put(self.CAPITALS_URL + str(capital_id), data = json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')
		
		r = requests.get(self.CAPITALS_URL + str(capital_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['capital'], m['capital'])
		self.assertEqual(resp['state'], m['state'])
		
	def test_capitals_delete_key(self):
		self.reset_data()
		capital_id = 6
		
		m = {}
		r = requests.delete(self.CAPITALS_URL + str(capital_id), data = json.dumps(m))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')
		
		r = requests.get(self.CAPITALS_URL + str(capital_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
	unittest.main()
