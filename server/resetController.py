import cherrypy
import re, json
from cap_library import _capital_database

class ResetController(object):

	def __init__(self, cdb=None):
		if cdb is None:
			self.cdb = _capital_database()
		else:
			self.cdb = cdb

	def PUT_INDEX(self):
		output = {'result' : 'success'}

		data = json.loads(cherrypy.request.body.read().decode())

		self.cdb.__init__()
		self.cdb.load_capitals('capitals.dat')
		self.cdb.load_pop('populations.dat')

		return json.dumps(output)

	def PUT_KEY(self, capital_id):
		output = {'result' : 'success'}
		cid = int(capital_id)

		try:
			data = json.loads(cherrypy.request.body.read().decode())

			tempCdb = _capital_database()
			tempCdb.load_capitals('capitals.dat')

			pair = tempCdb.get_pair(cid)

			self.cdb.set_capital(cid, pair)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)
