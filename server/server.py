import routes
import cherrypy
from capController import CapController
from popController import PopController
from resetController import ResetController
from cap_library import _capital_database

def startService():
	
	dispatcher = cherrypy.dispatch.RoutesDispatcher()

	cdb = _capital_database()
	
	capController = CapController(cdb=cdb)
	resetController = ResetController(cdb=cdb)
	popController = PopController(cdb=cdb)

	dispatcher.connect('capital_get','/capitals/:capital_id', controller=capController, action='GET_KEY', conditions=dict(method=['GET']))
	dispatcher.connect('capital_put','/capitals/:capital_id', controller=capController, action='PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('capital_delete','/capitals/:capital_id', controller=capController, action='DELETE_KEY', conditions=dict(method=['DELETE']))
	dispatcher.connect('capital_index_get','/capitals/', controller=capController, action='GET_INDEX', conditions=dict(method=['GET']))
	dispatcher.connect('capital_index_post','/capitals/', controller=capController, action='POST_INDEX', conditions=dict(method=['POST']))
	dispatcher.connect('capital_index_delete','/capitals/', controller=capController, action='DELETE_INDEX', conditions=dict(method=['DELETE']))

	dispatcher.connect('reset_put', '/reset/:capital_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

	dispatcher.connect('population_get','/populations/:capital_id', controller=popController, action='GET_KEY', conditions=dict(method=['GET']))

	conf = {
			'global': {
				'server.thread_pool': 5,
				'server.socket_host': 'localhost',
				'server.socket_port': 51022
			},
			'/': {
				'request.dispatch': dispatcher,
				'tools.CORS.on': True,
				}
	}
	
	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
	startService()
