import unittest
import requests
import json

class TestIndex(unittest.TestCase):

    SITE_URL = 'http://localhost:51022' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    CAPITALS_URL = SITE_URL + '/capitals/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_index_get(self):
        self.reset_data()
        r = requests.get(self.CAPITALS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
		
        testCapital = {}
        capitals = resp['capitals']
        for cap in capitals:
            if cap['id'] == 6:
                testCapital = cap

        self.assertEqual(testCapital['capital'], 'Denver')
        self.assertEqual(testCapital['state'], 'Colorado')

    def test_index_post(self):
        self.reset_data()

        m = {}
        m['capital'] = 'Washington DC'
        m['state'] = 'USA'
        r = requests.post(self.CAPITALS_URL, data = json.dumps(m))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['id'], 51)

        r = requests.get(self.CAPITALS_URL + str(resp['id']))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['capital'], m['capital'])
        self.assertEqual(resp['state'], m['state'])

    def test_index_delete(self):
        self.reset_data()

        m = {}
        r = requests.delete(self.CAPITALS_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.CAPITALS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        capitals = resp['capitals']
        self.assertFalse(capitals)

if __name__ == "__main__":
    unittest.main()
