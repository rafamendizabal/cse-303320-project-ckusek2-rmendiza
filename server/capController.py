import cherrypy
import re, json
from cap_library import _capital_database

class CapController(object):

	def __init__(self, cdb=None):
		if cdb is None:
			self.cdb = _capital_database()
		else:
			self.cdb = cdb

		self.cdb.load_capitals('capitals.dat')

	def GET_KEY(self, capital_id):
		output = {'result' : 'success'}
		capital_id = int(capital_id)

		try:
			pair = self.cdb.get_pair(capital_id)
			if pair is not None:
				output['id'] = capital_id
				output['capital'] = pair[0]
				output['state'] = pair[1]

			else:
				output ['result'] = 'error'
				output['message'] = 'capital not found'

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def PUT_KEY(self, capital_id):
		output = {'result' : 'success'}
		capital_id = int(capital_id)

		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		pair = list()
		pair.append(data['capital'])
		pair.append(data['state'])

		self.cdb.set_capital(capital_id, pair)

		return json.dumps(output)

	def DELETE_KEY(self, capital_id):
		
		output = {'result' : 'success'}

		capital_id = int(capital_id)

		try:
			self.cdb.delete_capital(capital_id)
		except Exception as ex:
			output['result'] = 'failure'
			output['message'] = str(ex)

		return json.dumps(output)

	def GET_INDEX(self):
		output = {'result' : 'success'}
		output['capitals'] = []

		try:
			for cid in self.cdb.get_capitals():
				pair = self.cdb.get_pair(cid)
				cpair = {'id': cid, 'capital' : pair[0], 'state' : pair[1]}
				output['capitals'].append(cpair)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def POST_INDEX(self):
		output = { 'result' : 'success'}
		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		try:
			capitals = sorted(list(self.cdb.get_capitals()))
			newID = int(capitals[-1]) + 1
			self.cdb.capitals[newID] = data['capital']
			self.cdb.states[newID] = data['state']
			self.cdb.capital_pop[newID] = 0
			output['id'] = newID

		except Exception as ex:
			output['result'] = 'failure'
			output['message'] = str(ex)

		return json.dumps(output)

	def DELETE_INDEX(self):
		output = {'result' : 'success'}

		try:
			allCapitals = list(self.cdb.get_capitals())
			for cid in allCapitals:
				self.cdb.delete_capital(cid)

		except Exception as ex:
			output['result'] ='failure'
			output['message'] = str(ex)

		return json.dumps(output)
