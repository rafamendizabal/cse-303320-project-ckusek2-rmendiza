import unittest
import requests
import json

class TestReset(unittest.TestCase):

	SITE_URL = 'http://localhost:51022'
	print('Testing for server: ' + SITE_URL)
	RESET_URL = SITE_URL + '/reset/'

	def test_put_reset_index(self):
		m = {}
		r = requests.put(self.RESET_URL, json.dumps(m))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		r = requests.get(self.SITE_URL + '/capitals/')
		resp = json.loads(r.content.decode())
		capitals = resp['capitals']
		self.assertEqual(capitals[5]['capital'], 'Denver')

	def test_put_reset_key(self):
		m = {}
		r = requests.put(self.RESET_URL, json.dumps(m))

		capital_id = 6
		m['capital'] = 'A New Capital'
		m['state'] = 'A New State'
		r = requests.put(self.SITE_URL + '/capitals/' + str(capital_id), data=json.dumps(m))

		m = {}
		r = requests.put(self.RESET_URL + str(capital_id), data=json.dumps(m))
		resp = json.loads(r.content.decode('utf-8'))	
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.SITE_URL + '/capitals/')
		resp = json.loads(r.content.decode('utf-8'))	
		capitals = resp['capitals']
		self.assertEqual(capitals[6]['capital'], 'Denver')

if __name__ == '__main__':
	unittest.main()
